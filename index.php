<?php include('includes/common.php'); ?>

<!DOCTYPE html>
<html>
<head>
	<title>HomePage | Git Learning</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>

<?php include('includes/header.php'); ?>

<div class="container">
	<div class="jumbotron">
	  <h1>Learn Github</h1>
	  <p>A Simple Learning of github in a very simple way with the industry base example.</p>
	  <p><a class="btn btn-primary btn-lg" href="topics.php">See Topics</a></p>
	</div>
</div>

<script src="js/index.js"></script>
</body>
</html>