<?php include('includes/common.php'); ?>
<!DOCTYPE html>
<html>
<head>
	<title>HomePage | Git Learning</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>

<?php include('includes/header.php'); ?>

<div class="container">
	<table class="table table-border table-hover ">
	  <thead>
	    <tr>
	      <th>#</th>
	      <th>Topic Title</th>
	      <th>Action</th>
	    </tr>
	  </thead>
	  <tbody>
	    <tr>
	      <td>1</td>
	      <td>Github Branch</td>
	      <td><a href="" class="btn btn-sm btn-success">View</a></td>
	    </tr>
	  </tbody>
	</table>
</div>

<script src="js/index.js"></script>
</body>
</html>